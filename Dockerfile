FROM ubuntu:bionic 

ENV TZ=Europe/Zurich 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone 
#RUN apt-get update && apt -y upgrade && apt install -y \
RUN apt-get update && apt install -y \
  gcc \
  automake \ 
  make \ 
  autopoint \ 
  libgcrypt20 \ 
  libgcrypt20-dev \ 
  texinfo \ 
  libltdl-dev \ 
  libgpg-error-dev \ 
  libidn11-dev \ 
  libunistring-dev \ 
  libglpk-dev \ 
  libbluetooth-dev \ 
  libextractor-dev \ 
  libmicrohttpd-dev \ 
  libgnutls28-dev \ 
  libjansson4 \
  libjansson-dev \ 
  libcurl4-gnutls-dev \ 
  libpq-dev \
  postgresql \ 
  libmicrohttpd12 \ 
  libtool \ 
  libltdl-dev \ 
  autoconf \ 
  libunistring-dev \ 
  libidn11-dev \ 
  wget \
  git 
  
  
RUN groupadd -g 1001 twister && useradd twister -g twister -u 1001 -m -d /home/twister  
USER twister 
WORKDIR /home/twister 

# install microhttpd
RUN wget https://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-0.9.62.tar.gz && tar -xzf libmicrohttpd-0.9.62.tar.gz && cd libmicrohttpd-0.9.62 && ./configure --prefix=$HOME/mc && make install 

# install gnunet
RUN cd $HOME && git clone git://git.gnunet.org/gnunet && cd gnunet && ./bootstrap && ./configure --prefix=$HOME/mc --with-microhttpd=$HOME/mc && make install  

# install exchange
RUN git clone git://git.taler.net/exchange && cd exchange && ./bootstrap && ./configure --prefix=$HOME/mc --with-gnunet=$HOME/mc --with-microhttpd=$HOME/mc && make install && cd .. 

# install twister
RUN git clone git://git.taler.net/twister && cd twister && ./bootstrap && ./configure --prefix=$HOME --with-gnunet=$HOME/mc --with-exchange=$HOME/mc --with-microhttpd=$HOME/mc && make install

VOLUME /home/twister/conf
#COPY twister.conf /home/twister/conf/twister.conf 

EXPOSE 8888 

CMD ["/home/twister/bin/taler-twister-service", "-L", "DEBUG", "-c", "/home/twister/conf/twister.conf"] 
