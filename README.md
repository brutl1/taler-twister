# taler-twister docker image 

## Build 
```
docker build -t myfancyimagename . 
```

## Usage 

Replace IP and PORT in conf/twister.conf 

```
docker run -p 8888:8888 -v conf:/home/twister/conf myfancyimagename
```
